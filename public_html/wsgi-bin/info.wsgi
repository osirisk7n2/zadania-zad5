#!/usr/bin/python
# -*- coding=utf-8 -*-

import Cookie
import datetime
import bookdb
import urlparse
def lookups(environ):
    try:
        return environ.get('REMOTE_HOST')
    except KeyError:
        return "no dns lookusp"

def createCookie():
    cookie=Cookie.SimpleCookie()
    cookie['visit']=datetime.datetime.today()
    return 'Set-Cookie: ', cookie.output().replace('Set-Cookie: ', '', 1)


body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
        <br>
        Ostatnia twoja wizyta miała miejsce %s.<br>
    </body>
</html>
"""

def application(environ, start_response):
    cookie_str=environ.get('HTTP_COOKIE')
    response_body = body % (
         environ.get('SERVER_NAME', 'Unset'), # nazwa serwera
         environ.get('SERVER_ADDR'), # IP serwera
         environ.get('SERVER_PORT'), # port serwera
         lookups(environ), # nazwa klienta
         environ.get('REMOTE_PORT'), # IP klienta
         environ.get('REMOTE_ADDR'), # port klienta
         environ.get('SCRIPT_NAME'), # nazwa skryptu
         datetime.datetime.today(), # bieżący czas
         cookie_str[8:], # czas ostatniej wizyty
    )
    status = '200 OK'



    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body))),createCookie()]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    app = application
    srv = make_server('', 31017, app)
    srv.serve_forever()