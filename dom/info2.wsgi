#!/usr/bin/python
# -*- coding=utf-8 -*-

import Cookie
import datetime
import bookdb
import urlparse
def lookups(environ):
    try:
        return environ.get('REMOTE_HOST')
    except KeyError:
        return "no dns lookusp"

def createCookie():
    cookie=Cookie.SimpleCookie()
    cookie['visit']=datetime.datetime.today()
    return 'Set-Cookie: ', cookie.output().replace('Set-Cookie: ', '', 1)


body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zadanie domowe PWI</title>
    </head>
    <body>


"""

def application(environ, start_response):
    cookie_str=environ.get('HTTP_COOKIE')
    response_body = body

    d = urlparse.parse_qs(environ['QUERY_STRING'])
    print(environ['QUERY_STRING']+"string")
    bookId=d.get('id', [''])[0]

    status = '200 OK'
    #####nasz request, jesli nie podalismy zadnego ID ksiazki to renderujemy cala baze danych
    baza=bookdb.BookDB()
    if bookId is "":
        response_body+="<ul>"
        for ksiazka in baza.titles():
            response_body+="<li>"+"<a href=\"http://194.29.175.240:31017/~p17/wsgi-bin/info2.wsgi?id="+ksiazka['id']+"\">"+ \
                           ksiazka['title']+ "</a>" +"</li>"

    #W przeciwnym wypadku dajemy userowi ksiazke
    else:
        try:
            ksiazka=baza.title_info(bookId)
            response_body+=""" Tytul: %s <br>
                        Isbn: %s <br>
                        Wydawca: %s <br>
                        Autor: %s <br>"""%(ksiazka['title'],ksiazka['isbn'],ksiazka['publisher'],
                                           ksiazka['author'])
            response_body+="<a href=\"http://194.29.175.240:31017/~p17/wsgi-bin/info2.wsgi\">Wróć do listy</a>"
        #No chyba ze takiej ksiazki nie ma : P
        except KeyError:
            status="404 not found"
            response_body+="Nie znaleziono takiej ksiazki :'("



    response_body+="</ul>"




    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)
    response_body+="""    </body>
</html>"""
    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    app = application
    srv = make_server('', 31017, application)
    srv.serve_forever()